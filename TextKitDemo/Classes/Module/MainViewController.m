//
//  MainViewController.m
//  TextKitDemo
//
//  Created by Admin on 15/9/1.
//  Copyright (c) 2015年 yulei. All rights reserved.
//

#import "MainViewController.h"
#import "NSStringUtility.h"
#import "OwnLabel.h"
#import "NSString+Expand.h"
#import "UILabel+Expand.h"

@interface MainViewController ()

@property (strong, nonatomic) OwnLabel *textLabel;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setTitle:@"文字图片混排"];
    [self layoutViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Main

- (void)layoutViews {
    
    CGRect textContent = CGRectMake(20.0f, 80.0f, 240.0f, 140.0f);
    [self.textLabel setFrame:textContent];
    [self.view addSubview:self.textLabel];

    NSString *content = @"文字加上表情[微笑][奋斗][鄙视][鼓掌][委屈][晕][哈哈][衰]";
//    //NSMutableAttributedString *attrbutedStr = [[NSMutableAttributedString alloc] initWithString:content];
//    //NSDictionary *contentDict = @{NSBaselineOffsetAttributeName: @(-10)};
//    //[attrbutedStr setAttributes:contentDict range:NSMakeRange(0, content.length)];
    NSMutableAttributedString *attrbutedStr = [NSStringUtility emotionStrWithString:content];
    self.textLabel.attributedText = attrbutedStr;
    
    CGSize textSize = [self.textLabel contentSizeWithMaxSize:CGSizeMake(textContent.size.width, MAXFLOAT)];
    NSLog(@"textSize is %@",NSStringFromCGSize(textSize));
    
    NSLog(@"text is %@",self.textLabel.attributedText);
    CGRect textRect = [attrbutedStr boundingRectWithSize:CGSizeMake(textContent.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    NSLog(@"textRect is %@",NSStringFromCGRect(textRect));
    //textContent.size.height = ceilf(textSize.height);
    //[self.textLabel setFrame:textContent];
}

#pragma mark - Getter And Setter

- (OwnLabel *)textLabel {
    if (_textLabel == nil) {
        _textLabel = [[OwnLabel alloc] init];
        _textLabel.backgroundColor = [UIColor yellowColor];
        _textLabel.textAlignment = NSTextAlignmentLeft;
        _textLabel.verticalAlignment = TextVerticalAlignmentTop;
        _textLabel.font = [UIFont systemFontOfSize:20.0f];
        _textLabel.textColor = [UIColor blackColor];
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _textLabel;
}

@end
