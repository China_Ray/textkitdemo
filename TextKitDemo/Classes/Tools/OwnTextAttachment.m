//
//  OwnTextAttachment.m
//  TextKitDemo
//
//  Created by Admin on 15/9/7.
//  Copyright (c) 2015年 yulei. All rights reserved.
//

#import "OwnTextAttachment.h"

@implementation OwnTextAttachment

- (CGRect)attachmentBoundsForTextContainer:(NSTextContainer *)textContainer proposedLineFragment:(CGRect)lineFrag glyphPosition:(CGPoint)position characterIndex:(NSUInteger)charIndex {
    return CGRectMake(0, 0, lineFrag.size.height, lineFrag.size.height);
}

@end
