//
//  NSStringUtility.h
//  TextKitDemo
//  功能描述 - 文字图片混排处理
//  Created by Admin on 15/9/1.
//  Copyright (c) 2015年 yulei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSStringUtility : NSObject

//处理表情字符
+ (NSMutableAttributedString *)emotionStrWithString:(NSString *)_text;

@end
