//
//  NSStringUtility.m
//  TextKitDemo
//
//  Created by Admin on 15/9/1.
//  Copyright (c) 2015年 yulei. All rights reserved.
//

#import "NSStringUtility.h"
#import "OwnTextAttachment.h"

@import UIKit.NSTextAttachment;
@import UIKit.UIImage;

@implementation NSStringUtility

//处理表情字符
+ (NSMutableAttributedString *)emotionStrWithString:(NSString *)_text {
    if (_text == nil || [_text isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    NSDictionary *contentDict = @{NSBaselineOffsetAttributeName: @(0)};
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:_text attributes:contentDict];
    
    //查找是否存在本地表情相关文件
    NSString *emotionFilePath = [[NSBundle mainBundle] pathForResource:@"Emotions" ofType:@"plist"];
    NSDictionary *emotionDict = [NSDictionary dictionaryWithContentsOfFile:emotionFilePath];
    if (emotionDict == nil) {
        NSLog(@"emotion dict is null");
        return attributeStr;
    }
    
    //通过正则表达式匹配字符串
    NSError *error = nil;
    NSString *regex_emotion = @"\\[[\u4e00-\u9fa5]+\\]";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regex_emotion options:NSRegularExpressionCaseInsensitive error:&error];
    if (!regex) {
        NSLog(@"emotion regex error is %@",[error localizedDescription]);
        return attributeStr;
    }
    
    //返回符合正则表达式的表情数组
    NSArray *resultArray = [regex matchesInString:_text options:0 range:NSMakeRange(0, _text.length)];
    //新建数组，存放图片及图片位置的字典
    NSMutableArray *imageArray = [NSMutableArray arrayWithCapacity:resultArray.count];
    for (NSTextCheckingResult *match in resultArray) {
        //数组元素所在的range
        NSRange range = [match range];
        //原字符串中对应的值
        NSString *subStr = [_text substringWithRange:range];
        //表情图片对象
        NSString *imageStr = [emotionDict objectForKey:subStr];
        if (imageStr != nil) {
            //新建文字附件，并设置图片及图片bounds
            OwnTextAttachment *textAttachment = [[OwnTextAttachment alloc] init];
            textAttachment.image = [UIImage imageNamed:imageStr];
            //textAttachment.bounds = CGRectMake(0, -3.0f, textAttachment.image.size.width, textAttachment.image.size.height);
            //把文字附件转换成可变字符串
            NSAttributedString *imageAStr = [NSAttributedString attributedStringWithAttachment:textAttachment];
            //数组存放入字典
            NSMutableDictionary *imageDict = [NSMutableDictionary dictionaryWithCapacity:2];
            [imageDict setObject:imageAStr forKey:@"image"];
            [imageDict setObject:[NSValue valueWithRange:range] forKey:@"range"];
            //把字典存入数组 
            [imageArray addObject:imageDict];
        }
    }
    
    //从后往前替换，否则会引起位置问题
    for (int i = (int)imageArray.count-1; i >= 0; i--) {
        NSRange range;
        [imageArray[i][@"range"] getValue:&range];
        //替换
        [attributeStr replaceCharactersInRange:range withAttributedString:imageArray[i][@"image"]];
    }
    
    return attributeStr;
}

@end
